package com.paula.kakol.crud;

import com.paula.kakol.crud.order.Order;
import com.paula.kakol.crud.order.OrderRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
public class OrderRepositoryTests {
    @Autowired
    private OrderRepository repo;

    @Test
    public void testAddNew() {
        Order order = new Order();
        order.setIndustry("Architektura");
        order.setWorkType("Projekt indywidualny budynku jednorodzinnego");
        order.setLocation("Obręb Rębiechowo, dz. nr 487/15");
        order.setCustomer("Jan Kowalski");
        order.setPhone("111222333");
        order.setEmail("jan@kowalski.pl");
        order.setCost(10000.00);
        LocalDate localDate = LocalDate.of(2023, 1, 2);
        Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        order.setDate(date);

        Order savedOrder = repo.save(order);

        Assertions.assertThat(savedOrder).isNotNull();
        Assertions.assertThat(savedOrder.getId()).isGreaterThan(0);
    }

    @Test
    public void testListAll() {
        repo.findAll();
        Iterable<Order> orders = repo.findAll();
        Assertions.assertThat(orders).hasSizeGreaterThan(0);

        for (Order order : orders) {
            System.out.println(order);
        }
    }

    @Test
    public void testUpdate() {
        Integer orderId = 1;
        Optional<Order> optionalOrder = repo.findById(orderId);
        Order order = optionalOrder.get();
        order.setEmail("kowalski@jan.pl");
        repo.save(order);

        Order updateOrder = repo.findById(orderId).get();
        Assertions.assertThat(updateOrder.getEmail()).isEqualTo("kowalski@jan.pl");
    }

    @Test
    public void testGet() {
        Integer orderId = 1;
        Optional<Order> optionalOrder = repo.findById(orderId);
        Assertions.assertThat(optionalOrder).isPresent();
        System.out.println(optionalOrder.get());
    }

    @Test
    public void testDelete() {
        Integer orderId = 1;
        repo.deleteById(orderId);
        Optional<Order> optionalOrder = repo.findById(orderId);
        Assertions.assertThat(optionalOrder).isNotPresent();
    }
}
