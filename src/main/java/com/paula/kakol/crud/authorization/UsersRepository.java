package com.paula.kakol.crud.authorization;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<UsersModel, Integer> {
    Optional<UsersModel> findByLoginAndPassword(String login, String password);

    boolean existsByLogin(String login);

    Optional<Object> findByLogin(String login);
}
