package com.paula.kakol.crud.authorization;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsersService {

    private final UsersRepository usersRepository;
    private final BCryptPasswordEncoder passwordEncoder;


    public UsersService(UsersRepository usersRepository, BCryptPasswordEncoder passwordEncoder) {
        this.usersRepository = usersRepository;
        this.passwordEncoder = passwordEncoder;
    }


    public UsersModel registerUser(String login, String password, String email){
        if (login == null || password == null) {
            return null;
        } else {
            if (usersRepository.existsByLogin(login)) {
                throw new RuntimeException("Użytkownik o podanym loginie już istnieje");
            }
            UsersModel usersModel = new UsersModel();
            usersModel.setLogin(login.toLowerCase());
            String hashedPassword = passwordEncoder.encode(password);
            usersModel.setPassword(hashedPassword);
            usersModel.setEmail(email.toLowerCase());
            return usersRepository.save(usersModel);
        }
    }

    public UsersModel authenticate(String login, String password) {
        UsersModel user = (UsersModel) usersRepository.findByLogin(login).orElse(null);
        if (user != null) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            if (passwordEncoder.matches(password, user.getPassword())) {
                return user;
            }
        }
        return null;
    }



}
