package com.paula.kakol.crud.order;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Integer> {
     public Long countById(Integer id);

     @Query("SELECT o FROM Order o WHERE o.industry = :industry")
     List<Order> getOrdersByIndustry(String industry);

     @Query("SELECT SUM(o.cost) FROM Order o WHERE o.date >= :startDate AND o.date <= :endDate AND o.industry = :industry")
     BigDecimal calculateTotalCostByDateRangeAndIndustry(@Param("startDate") Date startDate,
                                                         @Param("endDate") Date endDate,
                                                         @Param("industry") String industry);

}
