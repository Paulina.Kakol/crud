package com.paula.kakol.crud.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    @Autowired
    private OrderRepository repo;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderRepository orderService;

    public List<Order> listAll() {
        return (List<Order>) repo.findAll();
    }

    public void save(Order order) {
        repo.save(order);
    }

    public Order get(Integer id) throws OrderNotFoundException {
        Optional<Order> result = repo.findById(id);
        if (result.isPresent()) {
            return result.get();
        }
        throw new OrderNotFoundException("Nie można odnaleźć żadnych zleceń po ID " + id);
    }

    public void delete(Integer id) throws OrderNotFoundException {
        Long count = repo.countById(id);
        if(count == null || count == 0) {
            throw new OrderNotFoundException("Nie można odnaleźć żadnych zleceń po ID " + id);
        }
        repo.deleteById(id);
    }

    public List<Order> getOrdersByIndustry(String industry) {
        List<Order> orders = new ArrayList<>();

        for (Order order : orderRepository.findAll()) {
            if (order.getIndustry().equals(industry)) {
                orders.add(order);
            }
        }

        return orders;
    }

    public BigDecimal calculateTotalCostByDateRangeAndIndustry(Date startDate, Date endDate, String industry) {
        if (industry.equals("Geodezja")) {
            return orderRepository.calculateTotalCostByDateRangeAndIndustry(startDate, endDate, "Geodezja");
        } else if (industry.equals("Architektura")) {
            return orderRepository.calculateTotalCostByDateRangeAndIndustry(startDate, endDate, "Architektura");
        } else {
            return BigDecimal.ZERO;
        }
    }
}
