package com.paula.kakol.crud.order;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 20)
    private String industry;

    @Column(nullable = false, length = 100, name = "work_type")
    private String workType;

    @Column(nullable = false, length = 100, name = "location")
    private String location;

    @Column(nullable = false, length = 100)
    private String customer;

    @Column(nullable = false, length = 15)
    private String phone;

    @Column(length = 45)
    private String email;

    @Column(nullable = false, length = 15)
    private Double cost;

    @Column(nullable = false, columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", industry='" + industry + '\'' +
                ", workType='" + workType + '\'' +
                ", location='" + location + '\'' +
                ", customer='" + customer + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", cost=" + cost +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(id, order.id) && industry.equals(order.industry) && Objects.equals(workType, order.workType) && Objects.equals(location, order.location) && Objects.equals(customer, order.customer) && Objects.equals(phone, order.phone) && Objects.equals(email, order.email) && Objects.equals(cost, order.cost) && Objects.equals(date, order.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, industry, workType, location, customer, phone, email, cost, date);
    }
/////////////// gettery settery ///////////////

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

