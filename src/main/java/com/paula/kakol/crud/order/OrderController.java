package com.paula.kakol.crud.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

@Controller
public class OrderController {
    @Autowired
    private OrderService service;
    @Autowired
    private OrderService orderService;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private final OrderRepository orderRepository;

    @GetMapping("/orders")
    public String showOrderList(Model model, @RequestParam(value = "industry", required = false) String industry) {
        List<Order> listOrders;
        if (industry != null && (industry.equals("Architektura") || industry.equals("Geodezja"))) {
            listOrders = service.getOrdersByIndustry(industry);
        } else {
            listOrders = service.listAll();
        }

        Collections.sort(listOrders, Comparator.comparing(Order::getId).reversed());
        model.addAttribute("listOrders", listOrders);

        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.getDefault());
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(' ');

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00 PLN", symbols);
        model.addAttribute("decimalFormat", decimalFormat);

        return "orders";
    }

    @GetMapping("/orders/new")
    public String showNewForm(Model model){
        model.addAttribute("order", new Order());
        model.addAttribute("pageTitle", "Dodawanie Nowego Zlecenia");
        return "order_form";
    }

    @PostMapping("/orders/save")
    public String saveOrder(Order order, RedirectAttributes ra) {
        service.save(order);
        ra.addFlashAttribute("message", "Zmiany zostały zapisane.");
        return "redirect:/orders";
    }

    @GetMapping("/orders/edit/{id}")
        public String showEditForm(@PathVariable("id") Integer id, Model model, RedirectAttributes ra) {
        try {
            Order order = service.get(id);
            model.addAttribute("order", order);
            model.addAttribute("pageTitle", "Edytowanie Zlecenia [ID: " + id + "]");
            return "order_form";
        } catch (OrderNotFoundException e) {
            ra.addFlashAttribute("message", e.getMessage());
            return "redirect:/orders";
        }
    }

    @GetMapping("/orders/delete/{id}")
    public String deleteOrder(@PathVariable("id") Integer id, RedirectAttributes ra) {
        try {
            service.delete(id);
        } catch (OrderNotFoundException e) {
            ra.addFlashAttribute("message", e.getMessage());
        }
        return "redirect:/orders";
    }
    public OrderController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @GetMapping("/orders/select")
    public String showOrdersByIndustry(Model model, @RequestParam("industry") String industry) {
        List<Order> orders = service.getOrdersByIndustry(industry);

        model.addAttribute("listOrders", orders);

        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.getDefault());
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(' ');

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00 PLN", symbols);
        model.addAttribute("decimalFormat", decimalFormat);

        return "orders";
    }

    @GetMapping("/orders/costs")
    public String calculateTotalCost(Model model) {
        model.addAttribute("startDate", LocalDate.now());
        model.addAttribute("endDate", LocalDate.now());
        model.addAttribute("industries", Arrays.asList("Geodezja", "Architektura"));
        return "orders_costs";
    }

    @PostMapping(value = "/orders/costs", produces = "text/plain")
    @ResponseBody
    public String handleTotalCostCalculation(
            @RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
            @RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate,
            @RequestParam("industry") List<String> industries,
            Model model) {

        BigDecimal totalCost = BigDecimal.ZERO;
        for (String industry : industries) {
            totalCost = totalCost.add(orderService.calculateTotalCostByDateRangeAndIndustry(startDate, endDate, industry));
        }

        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.getDefault());
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(' ');

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00 PLN", symbols);
        model.addAttribute("decimalFormat", decimalFormat);

        return totalCost.toString();
    }

}
