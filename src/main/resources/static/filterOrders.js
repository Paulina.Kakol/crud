  function filterOrders(industry) {
    var architekturaBtn = document.getElementById("architekturaBtn");
    var geodezjaBtn = document.getElementById("geodezjaBtn");
    architekturaBtn.classList.remove("active");
    geodezjaBtn.classList.remove("active");

    if (industry === 'Architektura') {
      architekturaBtn.classList.add("active");
    } else if (industry === 'Geodezja') {
      geodezjaBtn.classList.add("active");
    }

    window.location.href = "/orders?industry=" + industry;
  }

  