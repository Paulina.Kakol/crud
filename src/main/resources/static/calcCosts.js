$(document).ready(function() {
  $('#calcForm').submit(function(event) {
    event.preventDefault();

    var formData = $(this).serialize();

    fetch('/orders/costs', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: formData
    })
    .then(response => response.text())
    .then(data => {
      var result = parseFloat(data);

      if (!isNaN(result)) {
        var formattedResult = numeral(result).format('0.00') + ' PLN';

        $('#result').html('Całkowita suma wycen: ' + formattedResult);
      }
    })
    .catch(error => {
      console.error(error);
    });
  });
});
