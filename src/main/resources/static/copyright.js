(function() {
  var currentYear = new Date().getFullYear();
  var footerBar = document.createElement('div');
  footerBar.className = 'footer-bar';

  var copyrightElement = document.createElement('span');
  copyrightElement.className = 'copyright';
  copyrightElement.innerHTML = '&copy; ' + currentYear + ' Politechnika Gdańska | Paulina Kąkol';

  footerBar.appendChild(copyrightElement);
  document.body.appendChild(footerBar);

  var lastScrollTop = 0;
  window.addEventListener('scroll', function() {
    var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    if (scrollTop > lastScrollTop) {
      footerBar.classList.remove('sticky');
    } else {
      footerBar.classList.add('sticky');
    }
    lastScrollTop = scrollTop;
  });
})();

  